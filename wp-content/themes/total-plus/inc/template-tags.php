<?php

/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Total Plus
 */
if (!function_exists('total_plus_posted_on')) :

    /**
     * Prints HTML with meta information for the current post-date/time and author.
     */
    function total_plus_posted_on() {

        $posted_on = sprintf(esc_html_x('On %s', 'post date', 'total-plus'), esc_html(get_the_date('d M, Y')));

        $avatar = get_avatar(get_the_author_meta('ID'), 32);

        echo $avatar;

        $byline = sprintf(
                esc_html_x('By %s', 'post author', 'total-plus'), '<span class="author vcard"><a class="url fn n" href="' . esc_url(get_author_posts_url(get_the_author_meta('ID'))) . '">' . esc_html(get_the_author()) . '</a></span>'
        );

        $comment_count = get_comments_number(); // get_comments_number returns only a numeric value

        if (comments_open()) {
            if ($comment_count == 0) {
                $comments = __('0 Comments', 'total-plus');
            } elseif ($comment_count > 1) {
                $comments = $comment_count . __(' Comments', 'total-plus');
            } else {
                $comments = __('1 Comment', 'total-plus');
            }
            $comment_link = '<i class="mdi mdi-comment-processing-outline" aria-hidden="true"></i> <a class="comment-link" href="' . get_comments_link() . '">' . $comments . '</a>';
        } else {
            $comment_link = "";
        }

        echo '<span class="entry-author"> ' . $byline . '</span><span class="entry-date published updated">' . $posted_on . '</span><span class="entry-comment">' . $comment_link . '</span>'; // WPCS: XSS OK.
    }

endif;

if (!function_exists('total_plus_entry_footer')) :

    /**
     * Prints HTML with meta information for the categories, tags and comments.
     */
    function total_plus_entry_footer() {
        $total_plus_single_post_category = get_theme_mod('total_plus_single_post_category', true);
        $total_plus_single_post_tags = get_theme_mod('total_plus_single_post_tags', true);

        if ($total_plus_single_post_category || $total_plus_single_post_tags) {

            $categories_list = get_the_category_list(' ');
            if ($categories_list && $total_plus_single_post_category) {
                echo '<div class="entry-category">';
                echo '<span class="cat-title">' . esc_html__('Categories', 'total-plus') . '</span>';
                echo $categories_list;
                echo '</div>';
            }

            /* translators: used between list items, there is a space after the comma */
            $tags_list = get_the_tag_list('', '');
            if ($tags_list && $total_plus_single_post_tags) {
                echo '<div class="entry-tags">';
                echo '<span class="tags-title">' . esc_html__('Tags', 'total-plus') . '</span>';
                echo $tags_list;
                echo '</div>';
            }
        }
    }

endif;

if (!function_exists('total_plus_entry_category')) :

    function total_plus_entry_category() {
        $categories_list = get_the_category_list(', ');
        if ($categories_list && total_plus_categorized_blog()) {
            echo '<i class="icofont-book-mark"></i>' . $categories_list;
        }
    }

endif;

if (!function_exists('total_plus_entry_tag')) :

    function total_plus_entry_tag() {
        $tags_list = get_the_tag_list('<i class="icofont-tag"></i>', ', ');
        if ($tags_list && total_plus_categorized_blog()) {
            echo $tags_list;
        }
    }

endif;

if (!function_exists('total_plus_comment_link')) :

    function total_plus_comment_link() {
        $comment_count = get_comments_number(); // get_comments_number returns only a numeric value

        if (comments_open()) {
            if ($comment_count == 0) {
                $comments = __('0 Comments', 'total-plus');
            } elseif ($comment_count > 1) {
                $comments = $comment_count . __(' Comments', 'total-plus');
            } else {
                $comments = __('1 Comment', 'total-plus');
            }
            $comment_link = '<i class="mdi mdi-comment-processing-outline"></i><a class="comment-link" href="' . get_comments_link() . '">' . $comments . '</a>';
        } else {
            $comment_link = "";
        }

        return $comment_link;
    }

endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function total_plus_categorized_blog() {
    if (false === ( $all_the_cool_cats = get_transient('total_plus_categories') )) {
        // Create an array of all the categories that are attached to posts.
        $all_the_cool_cats = get_categories(array(
            'fields' => 'ids',
            'hide_empty' => 1,
            // We only need to know if there is more than one category.
            'number' => 2,
        ));

        // Count the number of categories that are attached to the posts.
        $all_the_cool_cats = count($all_the_cool_cats);

        set_transient('total_plus_categories', $all_the_cool_cats);
    }

    if ($all_the_cool_cats > 1) {
        // This blog has more than 1 category so total_plus_categorized_blog should return true.
        return true;
    } else {
        // This blog has only 1 category so total_plus_categorized_blog should return false.
        return false;
    }
}

/**
 * Flush out the transients used in total_plus_categorized_blog.
 */
function total_plus_category_transient_flusher() {
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    // Like, beat it. Dig?
    delete_transient('total_plus_categories');
}

add_action('edit_category', 'total_plus_category_transient_flusher');
add_action('save_post', 'total_plus_category_transient_flusher');
